extends "res://kerno/fenestroj/tipo_d1.gd"


#plenigi formularon - заполнить форму
func plenigi_formularon():
	var tree = $VBox/body_texture/HSplit/VBoxContainer/Tree
	tree.clear()
	var itemlist = $VBox/body_texture/HSplit/VBoxContainer2/ItemList2
	itemlist.clear()
	tree.set_hide_root(true)
	var test = tree.create_item(null,-1)
	test.set_text(0,'скрытый')
	var tree_sxipo = tree.create_item()
	tree_sxipo.set_text(0, Global.direktebla_objekto[Global.realeco-2]['nomo']['enhavo'])
	var konservejo_sxipo = Title.get_node("CanvasLayer/UI/konservejo/konservejo").\
		sercxo_konservejo(Global.direktebla_objekto[Global.realeco-2])
	if konservejo_sxipo:
		var tree_konservejo = tree.create_item(tree_sxipo)
		tree_konservejo.set_text(0, konservejo_sxipo['nomo']['enhavo'])
		tree_konservejo.set_metadata(0,konservejo_sxipo)
	# в станции выводим склад станции и остальные наши корабли в станции
	if Global.fenestro_stacio:
		var hangaro = tree.create_item(null,-1)
		hangaro.set_text(0,'Ангар')
		for ligilo in Global.fenestro_stacio.ligilo:
			if ligilo['tipo']['objId'] == 3:
				var temp = tree.create_item(hangaro)
				var str1 = ligilo['ligilo']['nomo']['enhavo']
				for posedanto in ligilo['ligilo']['posedanto']['edges']:
					str1 += ' - '+ posedanto['node']['posedantoUzanto']['retnomo']
				temp.set_text(0,str1)
		var tree_konservejo_stacio = tree.create_item(null, -1)
		tree_konservejo_stacio.set_text(0,'Склад станции')
#		нужно отправить указатель на станцию, в которой находимся
		# ищем "универсальный модуль станции" по связи "Связь с персоналией"
		var konservejo_stacio = Global.fenestro_stacio.sercxo_tipo(1, 29)
		if konservejo_stacio:
			var tree_konservejo = tree.create_item(tree_konservejo_stacio)
			tree_konservejo.set_text(0, konservejo_stacio['nomo']['enhavo'])
			tree_konservejo.set_metadata(0,konservejo_stacio)


func _on_Tree_cell_selected():
	var tree = $VBox/body_texture/HSplit/VBoxContainer/Tree
	var select = tree.get_next_selected(null)
	var konservejo = select.get_metadata(0)
	var _items = $VBox/body_texture/HSplit/VBoxContainer2/ItemList2
	_items.clear()
	if !konservejo:
		return null

	var i = 1
	var volumeno = 0
	# print('===konservejo=',konservejo.uuid)
	# enteno - содержа́ние (одной субстанции в другой)
	for enteno in konservejo['ligilo']['edges']:
		# print('===enteno=',enteno['node']['ligilo']['nomo']['enhavo'])
		if enteno['node']['tipo']['objId']==3: # Находится внутри
			_items.add_item(String(i) + ') ' + String(enteno['node']['ligilo']['nomo']['enhavo']) + 
				', внутренний объём:' + String(enteno['node']['ligilo']['volumenoInterna']) + 
				', внешний объём:' + String(enteno['node']['ligilo']['volumenoEkstera']) + 
				', объём хранения:' + String(enteno['node']['ligilo']['volumenoStokado'])
			)
			_items.set_item_metadata(i-1,enteno)
			volumeno += enteno['node']['ligilo']['volumenoStokado']
			i += 1
	if konservejo['volumenoInterna']:
		_items.add_item('Всего места: ' + String(konservejo['volumenoInterna']))
		_items.add_item('Свободно: ' + String(konservejo['volumenoInterna'] - volumeno))
	else:
		_items.add_item('Всего занято: ' + String(volumeno))


# перемещаем на станционный склад выделенную позицию
# transmeti - переставить, переложить
# - какой объект
# - на какой склад
func transmeti(uuid_objekto,uuid_konservejo):
	print('===uuid_objekto===',uuid_objekto)
	print('===uuid_konservejo===',uuid_konservejo)
	pass

# перемещаем на станционный склад выделенную позицию
func _on_toStacio_pressed():
	var _item = $VBox/body_texture/HSplit/VBoxContainer2/ItemList2
	var _items = _item.get_selected_items()
	if _items.size()>0:
		if _item.get_item_metadata(_items[0]):
			var uuid_objekto = _item.get_item_metadata(_items[0])['node']['ligilo']['uuid']
			print('== передающийся объект =',uuid_objekto)
			var konservejo_stacio = Global.fenestro_stacio.sercxo_tipo(1, 29)
			if konservejo_stacio:
				print('== куда положить =',konservejo_stacio['uuid'])





