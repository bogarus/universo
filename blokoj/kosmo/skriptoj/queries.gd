extends Object
# Здесь будем хранить всё для запросов к бэкэнду по блоку "Космос"


# устанавливаем проект
func instalo_projekto_json(objektoUuid, kom_koordX, kom_koordY, kom_koordZ,
	fin_koordX, fin_koordY, fin_koordZ, id):
	var tipoId = 2
	var kategorio = 3
	var statusoId = 2
	var nomo = "Movado"
	var priskribo = "Movado de objekto"
	var statusoPosedantoId = 1
	var tipoPosedantoId = 1
	if !id:
		id = Net.get_current_query_id()
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation ($tipoId:Int, $kategorio:[Int],'+
		' $nomo:String, $priskribo:String, $statusoId:Int, $kom_koordX:Float, '+
		' $kom_koordY:Float, $kom_koordZ:Float, $fin_koordX:Float, '+
		' $fin_koordY:Float, $fin_koordZ:Float, $tipoPosedantoId:Int,'+
		' $statusoPosedantoId:Int, $objektoUuid:String, '+
		' $realecoId:Int ) '+
		'{ redaktuUniversoProjekto ( '+
		' tipoId:$tipoId, kategorio:$kategorio, nomo:$nomo, '+
		' priskribo:$priskribo, statusoId:$statusoId, publikigo:true, komKoordinatoX:$kom_koordX,'+
		' komKoordinatoY:$kom_koordY, komKoordinatoZ:$kom_koordZ, finKoordinatoX:$fin_koordX,'+
		' finKoordinatoY:$fin_koordY, finKoordinatoZ:$fin_koordZ, posedantoTipoId:$tipoPosedantoId, '+
		' objektoUuid: $objektoUuid, posedantoStatusoId:$statusoPosedantoId ,'+
		' realecoId:$realecoId ) '+
		' { status message '+
		' universoProjekto { uuid } } }',
		'variables': {"tipoId":tipoId, "kategorio": kategorio, "nomo": nomo,
			"priskribo": priskribo, "statusoId": statusoId,
			"kom_koordX": kom_koordX, "kom_koordY": kom_koordY, "kom_koordZ": kom_koordZ, 
			"fin_koordX":fin_koordX, 
			"fin_koordY":fin_koordY, "fin_koordZ":fin_koordZ,
			"objektoUuid":objektoUuid, "statusoPosedantoId":statusoPosedantoId,
			"tipoPosedantoId":tipoPosedantoId, "realecoId":Global.realeco} }})
	# print('===instalo_projekto===',query)
	return query


# записываем список задач с их владельцами
func instalo_tasko_posedanto_koord(uuid, projekto_uuid, kom_koordX, kom_koordY, kom_koordZ, itineroj, id=0):
	# создаём список задач, создаём владельца проекта, устанавливаем координаты объекту
	var tipoId = 2
	var kategorio = []
	var statusoId = [2] # первая задача в работе
	var nomo = []
	var priskribo = []
	var objektoUuid = []
	var tipoPosedantoId = 1
	var statusoPosedantoId = 1
	var pozicio =[1]
	var fin_koordX = []
	var fin_koordY = []
	var fin_koordZ = []
	# параметры координат последующих задач
	var i = 0
	var komKoordinatoX=[]
	var komKoordinatoY=[]
	var komKoordinatoZ=[]
	for iti in itineroj:
		kategorio.append(itineroj[i]['kategorio'])
		nomo.append(itineroj[i]['nomo'])
		priskribo.append(itineroj[i]['priskribo'])
		if i==0:
			komKoordinatoX.append(kom_koordX)
			komKoordinatoY.append(kom_koordY)
			komKoordinatoZ.append(kom_koordZ)
		else:
			pozicio.append(i+1)
			komKoordinatoX.append(itineroj[i-1]['koordinatoX'])
			komKoordinatoY.append(itineroj[i-1]['koordinatoY'])
			komKoordinatoZ.append(itineroj[i-1]['koordinatoZ'])
			statusoId.append(1) # новая задача
		fin_koordX.append(itineroj[i]['koordinatoX'])
		fin_koordY.append(itineroj[i]['koordinatoY'])
		fin_koordZ.append(itineroj[i]['koordinatoZ'])
		if itineroj[i]['uuid_celo']:
			objektoUuid.append(itineroj[i]['uuid_celo'])
		else:
			objektoUuid.append(null)
		i += 1

	if !id:
		id = Net.get_current_query_id()
	var query = JSON.print({
		'type': 'start',
		'id': '%s' % id,
		'payload':{ 'query': 'mutation ( ' +
		'$tipoId:Int, $kategorio:[[Int]], $nomo:[String], $priskribo:[String], $statusoId:[Int], $projekto_uuid: String,' +
		'$komKoordinatoX:[Float], $komKoordinatoY:[Float], $komKoordinatoZ:[Float], $tipoPosedantoId:Int,' +
		'$fin_koordX:[Float], $fin_koordY:[Float], $fin_koordZ:[Float], $pozicio:[Int], $statusoPosedantoId:Int,' +
		'$objektoUuid:[String], $realecoId:Int, $posedantoObjektoUuid:String)'+
		' { redaktuKreiUniversoTaskojPosedanto (projektoUuid: $projekto_uuid, tipoId:$tipoId, kategorio:$kategorio, nomo:$nomo, '+
		' priskribo:$priskribo, statusoId:$statusoId, pozicio:$pozicio, komKoordinatoX:$komKoordinatoX,'+
		' komKoordinatoY:$komKoordinatoY, komKoordinatoZ:$komKoordinatoZ, finKoordinatoX:$fin_koordX,'+
		' finKoordinatoY:$fin_koordY, finKoordinatoZ:$fin_koordZ, posedantoStatusoId:$statusoPosedantoId,'+
		' posedantoTipoId:$tipoPosedantoId, objektoUuid:$objektoUuid, posedantoObjektoUuid:$posedantoObjektoUuid,'+
		' realecoId:$realecoId ) { status '+
		' message universoTaskoj { uuid } } }',
		'variables':  {"tipoId":tipoId, "kategorio": kategorio, "nomo": nomo, "priskribo": priskribo, 
		"statusoId": statusoId, "projekto_uuid": projekto_uuid, "fin_koordX":fin_koordX, 
		"fin_koordY":fin_koordY, "fin_koordZ":fin_koordZ, "tipoPosedantoId":tipoPosedantoId,
		"statusoPosedantoId":statusoPosedantoId, "objektoUuid":objektoUuid, "pozicio":pozicio,
		"komKoordinatoX":komKoordinatoX, "komKoordinatoY":komKoordinatoY, "komKoordinatoZ":komKoordinatoZ,
		"posedantoObjektoUuid":uuid,
		"realecoId":Global.realeco}}})
	# if Global.logs:
	# 	print('===instalo_tasko_posedanto_koord===',query)
	return query


# создаём задачу, устанавливаем координаты объекту, изменяем финальные координаты проекту
func instalo_tasko_koord_json(uuid, projekto_uuid, kom_koordX, kom_koordY, kom_koordZ,
		fin_koordX, fin_koordY, fin_koordZ, id=0):
	var posedantoTipoId = 1
	var posedantoStatusoId = 1
	var tipoId = 2
	var kategorio = 3 # категория движения объектов в космосе
	var statusoId = 2
	var nomo = "Movado"
	var priskribo = "Movado de objekto"
	if !id:
		id = Net.get_current_query_id()
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation ($koordX:Float, $koordY:Float, $koordZ:Float, '+
		'$tipoId:Int, $kategorio:[Int], $nomo:String, $priskribo:String, $statusoId:Int, $projekto_uuid: UUID,'+
		'$fin_koordX:Float, $fin_koordY:Float, $fin_koordZ:Float, $projektoUuid: String, '+
		'$posedantoTipoId:Int, $posedantoStatusoId:Int, $objektoUuid:String, $UzantoId:Int, '+
		'$realecoId:Int)'+
		'{ redaktuUniversoTaskoj (projektoUuid: $projektoUuid, tipoId:$tipoId, kategorio:$kategorio, nomo:$nomo, '+
		' priskribo:$priskribo, statusoId:$statusoId, publikigo:true, komKoordinatoX:$koordX,'+
		' komKoordinatoY:$koordY, komKoordinatoZ:$koordZ, finKoordinatoX:$fin_koordX,'+
		' finKoordinatoY:$fin_koordY, finKoordinatoZ:$fin_koordZ, posedantoTipoId:$posedantoTipoId, '+
		' objektoUuid:$objektoUuid, posedantoStatusoId:$posedantoStatusoId, '+
		' posedantoUzantoSiriusoUzantoId: $UzantoId, ' +
		' realecoId:$realecoId) '+
		'{ status '+
		' message universoTaskoj { uuid } } '+
		'redaktuUniversoProjekto ( uuid:$projekto_uuid ,'+
		' finKoordinatoX:$fin_koordX,'+
		' finKoordinatoY:$fin_koordY, finKoordinatoZ:$fin_koordZ ) '+
		' { status message '+
		' universoProjekto { uuid } } }',
		'variables': {"uuid":uuid, "koordX": kom_koordX, "koordY": kom_koordY, "koordZ": kom_koordZ,
		"tipoId":tipoId, "kategorio": kategorio, "nomo": nomo, "priskribo": priskribo, 
		"statusoId": statusoId, "projekto_uuid": projekto_uuid, "projektoUuid": projekto_uuid, 
		"fin_koordX":fin_koordX, "fin_koordY":fin_koordY, "fin_koordZ":fin_koordZ,
		"posedantoStatusoId":posedantoStatusoId, "posedantoTipoId":posedantoTipoId,
		"UzantoId":Global.id, 
		"objektoUuid":uuid, "realecoId":Global.realeco } }})
	# print('===instalo_tasko_koord===',query)
	return query


# подписка на действия в кубе
func kubo_json(id=0):
	if !id:
		id = Net.get_current_query_id()
	var statusoIdIn = '"'+String(Net.statuso_nova) + ', '+String(Net.statuso_laboranta) + ', '+String(Net.status_pauzo)+'"'
	var statusoId = String(Net.statuso_laboranta)
	var tipoId = String(Net.tasko_tipo_objekto)
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'subscription ($kuboj:[Int]!, $realeco:Int!)'+
		'{ universoObjektoEventoj (kuboj: $kuboj, realeco:$realeco) { evento '+
		' objekto { uuid koordinatoX koordinatoY koordinatoZ '+
		"  nomo { enhavo } priskribo { enhavo } "+
		"  integreco volumenoInterna volumenoEkstera volumenoStokado " +
		"  kubo{objId} " +
		"  posedantoId " +
		"  posedanto{edges{node{" +
		"   posedantoUzanto{ retnomo siriusoUzanto{ objId}}}}}" +
		"  stato{objId potenco integreco statoAntaua {integreco} "+
		"   statoSekva{integreco}}" +
		"  resurso{objId tipo{objId}}" +
		" ligiloLigilo{edges{node{ uuid "+
		"  tipo {objId} " +
		"  posedanto{ kubo {objId} koordinatoX koordinatoY " +
		" koordinatoZ uuid nomo {enhavo} }}}}" +
		"  ligilo{edges{node{ uuid"+
		"   posedantoStokejo {uuid}" +
		"   konektiloPosedanto konektiloLigilo tipo{objId}"+
		"   ligilo{ uuid integreco volumenoInterna volumenoEkstera volumenoStokado nomo{enhavo} resurso{objId} "+
		"    stato{objId potenco statoAntaua {integreco} "+
		"     statoSekva{integreco}}" +
		"    ligilo{edges{node{ uuid " +
		"     posedantoStokejo {uuid}" +
		"     konektiloPosedanto konektiloLigilo tipo{objId} " +
		"     ligilo{ uuid integreco volumenoInterna volumenoEkstera volumenoStokado " +
		"     nomo{enhavo} resurso{objId} }}}}}" +
		"     tipo{objId}}}}" +
		"  projekto (statuso_Id: "+statusoId+", tipo_Id: "+tipoId+"){ "+
		"   edges { node { uuid statuso{objId} "+
		"   kategorio {edges {node {objId nomo{enhavo}}}}" +
		"   tasko (statuso_Id_In: "+statusoIdIn+"){ edges {node { "+
		"    uuid finKoordinatoX finKoordinatoY finKoordinatoZ "+
		"    objekto{uuid} " +
		"    nomo{enhavo} priskribo{enhavo} " +
		"    kategorio {edges {node {objId nomo{enhavo}}}}" +
		"    pozicio statuso {objId} } } } } } } "+
		'  rotaciaX rotaciaY rotaciaZ } '+
		' projekto {uuid statuso{objId} kategorio {edges {node {objId }}}} '+
		' tasko { uuid komKoordinatoX komKoordinatoY '+
		"  nomo{enhavo} priskribo{enhavo} " +
		"  objekto{uuid} " +
		'  posedanto{edges{node{ posedantoObjekto{uuid} }}}' +
		'  komKoordinatoZ finKoordinatoX finKoordinatoY '+
		'  finKoordinatoZ pozicio statuso{objId} kategorio { '+
		'  edges { node { objId } } }} '+
		' mastro {uuid nomo{enhavo}}' +
		' posedi {uuid nomo {enhavo}}' +
		'} }',
		'variables': {"kuboj": Global.kubo, "realeco": Global.realeco } }})
	# if Global.logs:
	# 	print('=== kubo_json =query= ',query)
	return query


