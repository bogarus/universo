extends Spatial


const sxipo = preload("res://blokoj/kosmosxipoj/scenoj/sxipo_fremdulo.tscn")
const sxipo_modulo = preload("res://blokoj/kosmosxipoj/skriptoj/moduloj/sxipo.gd")


var objekto # данные станции
var ligilo = [] # всё, что есть в станции


func _ready():
	Global.fenestro_itinero.malplenigi_itinero()
	Global.fenestro_itinero.projekto_itineroj_uuid = ''
	Global.fenestro_stacio = self


func _on_CapKosmostacio_ready():
	# устанавливаем корабль на место 1А
	var ship = null
	ship = sxipo.instance()
	var sh = sxipo_modulo.new()
	sh.create_sxipo(ship, Global.direktebla_objekto[Global.realeco-2])

	ship.visible=true
	
#	ship.rotate_y(1.58)
	ship.rotate_y(deg2rad(-90))
	ship.translation.x = ship.translation.x + 24.4 # насколько въезжать в парковку
	ship.translation.z = ship.translation.z - 5 # в сторону от центра
	ship.translation.y = ship.translation.y + 4 # высота от пола
	add_child(ship,true)
	$camera.doni_observoj(ship)


func _on_CapKosmostacio_tree_exiting():
	Global.fenestro_stacio = null


# ищем в связях запись по типу связи и типу ресурса объекта
# и возвращаем найденный элемент массива
func sercxo_tipo(tipoLigilo, tipoResuro):
	for ligil in ligilo:
		if (ligil['tipo']['objId'] == tipoLigilo and
			ligil['ligilo']['resurso']['objId'] == tipoResuro):
			return ligil['ligilo']
	return null
