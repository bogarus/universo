extends Object
# Здесь будем хранить всё для запросов к бэкэнду по блоку "rajtigo"


# URL к API (авторизация)
const URL_AUTH = "https://t34.tehnokom.su/api/v1.1/registrado/"
# URL к API
const URL_DATA = "https://t34.tehnokom.su/api/v1.1/"


# Запрос авторизации
func auth_query(login, password):
	return JSON.print({ "query": "mutation { ensaluti(login: \"%s\", password: \"%s\") { status token message csrfToken uzanto { objId } } }" % [login, password] })


# Запрос никнейма
func get_nickname_query(id):
	return JSON.print({ "query": "query { universoUzanto(siriusoUzantoId: %s) { edges { node { uuid retnomo } } } }" % id })


# запрос на актуальную версию программы
func get_aktuala_versio():
	var query = JSON.print(
		{ "query": "query { "+
		" universoAplikoVersio ( publikigo:true, tipo_Id:1, aktuala:true) " +
		" {edges{node{ tipo{ nomo{enhavo}} "+
		"  numeroVersio numeroSubversio numeroKorektado }} } }"} )
	return query
