extends Object
# Здесь будем хранить всё для запросов к бэкэнду по блоку "Космические корабли"


# создаём проект стрельбы на сервере
func pafado_json(objektoUuid, id):
	var nomo = "Pafado"
	var priskribo = "Pafado de objekto"
	if !id:
		id = Net.get_current_query_id()
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation ($tipoId:Int, $kategorio:[Int],'+
		' $nomo:String, $priskribo:String, $statusoId:Int, '+
		' $tipoPosedantoId:Int,'+
		' $statusoPosedantoId:Int, $objektoUuid:String, '+
		' $realecoId:Int ) '+
		'{ redaktuUniversoProjekto ( '+
		' tipoId:$tipoId, kategorio:$kategorio, nomo:$nomo, '+
		' priskribo:$priskribo, statusoId:$statusoId, publikigo:true, '+
		' posedantoTipoId:$tipoPosedantoId, '+
		' objektoUuid: $objektoUuid, posedantoStatusoId:$statusoPosedantoId ,'+
		' realecoId:$realecoId ) '+
		' { status message '+
		' universoProjekto { uuid } } }',
		'variables': {"tipoId":Net.projekto_tipo_objekto, 
			"kategorio": Net.projekto_kategorio_pafado, 
			"nomo": nomo,
			"priskribo": priskribo, "statusoId": Net.statuso_laboranta,
			"objektoUuid":objektoUuid, "statusoPosedantoId":Net.statuso_posedanto,
			"tipoPosedantoId":Net.tipo_posedanto, "realecoId":Global.realeco} }})
	# print('===pafado_json===',query)
	return query


# отправляем создание задачи взятия в прицел на сервер
# uuidPafado - uuid проекта стрельбы, далее кто берёт в прицел, откуда, кого и по каким координатам
func celilo_json(uuidPafado, objektoUuid, kom_koordX, kom_koordY, kom_koordZ,
	uuidCelo, fin_koordX, fin_koordY, fin_koordZ, id):
	var nomo = "Celilo"
	var priskribo = "Celilo de objekto"
	if !id:
		id = Net.get_current_query_id()
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation ($tipoId:Int, $kategorio:[Int],'+
		' $nomo:String, $priskribo:String, $statusoId:Int, $kom_koordX:Float, '+
		' $kom_koordY:Float, $kom_koordZ:Float, $fin_koordX:Float, '+
		' $fin_koordY:Float, $fin_koordZ:Float, $tipoPosedantoId:Int,'+
		' $statusoPosedantoId:Int, $objektoUuid:String, '+
		' $realecoId:Int, $projektoUuid:String, $uuidCelo:String) '+
		'{ redaktuUniversoTaskoj ( '+
		' projektoUuid:$projektoUuid' +
		' tipoId:$tipoId, kategorio:$kategorio, nomo:$nomo, '+
		' priskribo:$priskribo, statusoId:$statusoId, publikigo:true, komKoordinatoX:$kom_koordX,'+
		' komKoordinatoY:$kom_koordY, komKoordinatoZ:$kom_koordZ, finKoordinatoX:$fin_koordX,'+
		' finKoordinatoY:$fin_koordY, finKoordinatoZ:$fin_koordZ, posedantoTipoId:$tipoPosedantoId, '+
		' posedantoObjektoUuid: $objektoUuid, posedantoStatusoId:$statusoPosedantoId, '+
		' realecoId:$realecoId, objektoUuid:$uuidCelo ) '+
		' { status message '+
		' universoTaskoj { uuid } } }', 
		'variables': {"tipoId":Net.projekto_tipo_objekto, 
			"kategorio": Net.tasko_kategorio_celilo, 
			"nomo": nomo,
			"priskribo": priskribo, "statusoId": Net.statuso_laboranta,
			"kom_koordX": kom_koordX, "kom_koordY": kom_koordY, "kom_koordZ": kom_koordZ, 
			"fin_koordX":fin_koordX, 
			"fin_koordY":fin_koordY, "fin_koordZ":fin_koordZ,
			"objektoUuid":objektoUuid, "statusoPosedantoId":Net.statuso_posedanto,
			"tipoPosedantoId":Net.tipo_posedanto, "realecoId":Global.realeco,
			"projektoUuid":uuidPafado, "uuidCelo":uuidCelo } }})
	# print('===celilo_json===',query)
	return query


# отправляем выстрел на сервер
# uuidPafado - uuid проекта стрельбы, далее кто стреляет (uuid лазера или другого оружия), откуда стреляет, в кого стреляет и куда стреляет
func pafo_json(uuidPafado, objektoUuid, kom_koordX, kom_koordY, kom_koordZ,
	uuidCelo, fin_koordX, fin_koordY, fin_koordZ, id):
	var nomo = "Pafo"
	var priskribo = "Pafo de objekto"
	if !id:
		id = Net.get_current_query_id()
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation ($tipoId:Int, $kategorio:[Int],'+
		' $nomo:String, $priskribo:String, $statusoId:Int, $kom_koordX:Float, '+
		' $kom_koordY:Float, $kom_koordZ:Float, $fin_koordX:Float, '+
		' $fin_koordY:Float, $fin_koordZ:Float, $tipoPosedantoId:Int,'+
		' $statusoPosedantoId:Int, $objektoUuid:String, '+
		' $realecoId:Int, $projektoUuid:String, $uuidCelo:String) '+
		'{ redaktuUniversoTaskoj ( '+
		' projektoUuid:$projektoUuid' +
		' tipoId:$tipoId, kategorio:$kategorio, nomo:$nomo, '+
		' priskribo:$priskribo, statusoId:$statusoId, publikigo:true, komKoordinatoX:$kom_koordX,'+
		' komKoordinatoY:$kom_koordY, komKoordinatoZ:$kom_koordZ, finKoordinatoX:$fin_koordX,'+
		' finKoordinatoY:$fin_koordY, finKoordinatoZ:$fin_koordZ, posedantoTipoId:$tipoPosedantoId, '+
		' posedantoObjektoUuid: $objektoUuid, posedantoStatusoId:$statusoPosedantoId, '+
		' realecoId:$realecoId, objektoUuid:$uuidCelo ) '+
		' { status message '+
		' universoTaskoj { uuid } } }', 
		'variables': {"tipoId":Net.projekto_tipo_objekto, 
			"kategorio": Net.tasko_kategorio_pafo, 
			"nomo": nomo,
			"priskribo": priskribo, "statusoId": Net.statuso_laboranta,
			"kom_koordX": kom_koordX, "kom_koordY": kom_koordY, "kom_koordZ": kom_koordZ, 
			"fin_koordX":fin_koordX, 
			"fin_koordY":fin_koordY, "fin_koordZ":fin_koordZ,
			"objektoUuid":objektoUuid, "statusoPosedantoId":Net.statuso_posedanto,
			"tipoPosedantoId":Net.tipo_posedanto, "realecoId":Global.realeco,
			"projektoUuid":uuidPafado, "uuidCelo":uuidCelo } }})
	# if Global.logs:
	# 	print('===pafo_json===',query)
	return query


# задаём координаты и угол поворота объекту
# отправляет только Godot-сервер
func objecto_mutation(uuid, koordX, koordY, koordZ, rotaciaX, rotaciaY, rotaciaZ, id=0):
	if !id:
		id = Net.get_current_query_id()
		Net.net_id_clear.append(id)
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation ($uuid:UUID, $koordX:Float, $koordY:Float, $koordZ:Float,'+
		' $rotaciaX:Float, $rotaciaY:Float, $rotaciaZ:Float )'+
		' { redaktuUniversoObjekto ( uuid: $uuid, koordinatoX: $koordX, koordinatoY: $koordY, '+
		'koordinatoZ: $koordZ, rotaciaX: $rotaciaX, rotaciaY: $rotaciaY, rotaciaZ: $rotaciaZ ) '+
		' { status message universoObjektoj { uuid } } }',
		'variables': {"uuid":uuid, "koordX": koordX, "koordY": koordY, "koordZ": koordZ,
		"rotaciaX": rotaciaX, "rotaciaY": rotaciaY, "rotaciaZ": rotaciaZ} }})
	return query



