extends Control


const QueryObject = preload("queries.gd")


var celilo_id = [] # id переданных запросов на сервер п
var celilo = [] # список целей
var celilo_uuid = [] # uuid задач прицеливания
var sxipo # указатель на управляемый корабль
var max_celilo = 1 # максимальное количество прицелов
var pafado_id # id запроса на создание проекта ведения огня


var id_pafo = [] # закрываем задачу стрельбы
var pafo_uuid = [] # uuid задач по стрельбе


func _ready():
	# подключаем сигнал для обработки входящих данных
	var err = Net.connect("input_data", self, "_on_data")
	if err:
		print('error = ',err)


# warning-ignore:unused_argument
func _physics_process(delta):
	# вывод целей на экран
#	if get_node_or_null('Control'):
	# если есть цель или был произведн выстрел и идёт перезарядка, то продолжаем считывать показатели перезарядки
	if (celilo.size()>0) or ($kanonadi/livero.value>0):
		$kanonadi/livero.value = sxipo.armiloj.front().livero
#		else:
#			$Control/celilo.set_visible(false)
#			$Control/celilo/label.text = ''


func _on_data():
	var i_data_server = 0
	for on_data in Net.data_server:
		var index_pafo = id_pafo.find(int(on_data['id']))
		var celilo_index = celilo_id.find(int(on_data['id']))
		if celilo_index>-1:
			# пришло сообщение, по установлению цели
			celilo_id.remove(celilo_index)
			Net.data_server.remove(i_data_server)
		elif index_pafo > -1: # находится в списке выстрелов, закрываем задачу
			pafo_uuid.push_back(on_data['payload']['data']['redaktuUniversoTaskoj']['universoTaskoj']['uuid'])
			id_pafo.remove(index_pafo)
			Net.data_server.remove(i_data_server)
		elif int(on_data['id']) == pafado_id:
			# пришло сообщение, по созданию проекта стрельбы
			sxipo.pafado_uuid = on_data['payload']['data']['redaktuUniversoProjekto']['universoProjekto']['uuid']
			send_celilo(sxipo.pafado_celo)
			pafado_id = null
			Net.data_server.remove(i_data_server)
		i_data_server += 1


func _on_kanonadi_pressed():
	if celilo.size()>0:
		if $kanonadi.pressed:
#			постановка цели оружию
#			sxipo.get_node("laser_gun").set_target(celilo.front())
			# находим оружие, которым стрелять
			for arm in sxipo.armiloj:
				pafo_server(arm, celilo.front())
		else:
			# закрываем задачу ведения огня
#			sxipo.get_node("laser_gun").set_target(null)
			for pafo in pafo_uuid:
				Net.send_json(Queries.finado_tasko(pafo, Net.statuso_fermo))
	else:
		$kanonadi.pressed = false
#		sxipo.get_node("laser_gun").pafado = false


# устанавливаем цель
func set_celilo(celo):
	# если количество максимальных прицеливаний больше взятых в прицел, тогда берём в прицел
	if max_celilo<=len(celilo):
		return false
	if !celo.get('uuid'):
		return false
	celilo.append(celo)
	$celilo.set_visible(true)
	$celilo/label.text = celilo.front().uuid
	if sxipo.pafado_uuid: # если проект стрельбы есть, сразу отправляем цель
		send_celilo(celo)
	else:
		# создаём проект ведения огня
		var q = QueryObject.new()
		pafado_id = Net.get_current_query_id()
		sxipo.pafado_celo = celo
		Net.send_json(q.pafado_json(
			Global.direktebla_objekto[Global.realeco-2]['uuid'],pafado_id
		))


# отправляем на сервер установку цели
func send_celilo(celo):
	var q = QueryObject.new()
	var id = Net.get_current_query_id()
	celilo_id.append(id)
	Net.send_json(q.celilo_json(
		sxipo.pafado_uuid,
		Global.direktebla_objekto[Global.realeco-2]['uuid'], # uuid объекта управления
		sxipo.translation.x, #kom_koordX
		sxipo.translation.y, #kom_koordY
		sxipo.translation.z, #kom_koordZ
		celo.uuid,
		celo.translation.x, #kom_koordX
		celo.translation.y, #kom_koordY
		celo.translation.z, #kom_koordZ
		id
	))


# отправляем задачу стрельбы на сервер (передаются объекты в космосе)
# armile - выстреливше оружие
# celo - цель
func pafo_server(armilo, celo):
	var q = QueryObject.new()
	var id = Net.get_current_query_id()
	id_pafo.append(id) 
	Net.send_json(q.pafo_json(
		sxipo.pafado_uuid,
		armilo.uuid, # uuid стреляющего
		sxipo.translation.x, #kom_koordX
		sxipo.translation.y, #kom_koordY
		sxipo.translation.z, #kom_koordZ
		celo.uuid,
		celo.translation.x, #kom_koordX
		celo.translation.y, #kom_koordY
		celo.translation.z, #kom_koordZ
		id
	))


