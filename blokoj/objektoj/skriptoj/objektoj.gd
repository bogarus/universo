extends "res://kerno/fenestroj/tipo_a1.gd"
# окно списка объектов


func _ready():
	var err = Title.connect("load_objekto", self, "_reload_objekto")
	if err:
		print('ошибка установки реакции на load_objekto = ',err)

# перезагружаем список объектов
func _reload_objekto():
	$"VBox/body_texture/ItemList".clear()
	FillItemList()

func FillItemList():
	# если загружен космос, то берём список из объектов космоса
	if $"/root".get_node_or_null('space'):
		var _space = $"/root".get_node('space')
		var _items = get_node("VBox/body_texture/ItemList")
		for ch in _space.get_children():
			if ch.is_in_group('create') and (not('directebla_sxipo' in ch)):
				_items.add_item(
					'('+String(int(
					_space.get_node('ship').translation.distance_to(ch.translation))
					)+') '+ch.objekto['nomo']['enhavo'], 
					null, 
					true
				)
	else:
	# Заполняет список найдеными объектами
		for item in Global.objektoj:
			if not item.has('distance'):
				item['distance'] = 0
			get_node("VBox/body_texture/ItemList").add_item(
				'('+String(int(item['distance']))+') '+item['nomo']['enhavo'], 
				null, 
				true
			)


func distance_to(trans):
#	for obj in ItemListContent:
	for obj in Global.objektoj:
		obj['distance'] = trans.distance_to(Vector3(obj['koordinatoX'],
			obj['koordinatoY'],obj['koordinatoZ']))
	var index_pos = $'VBox/body_texture/ItemList'.get_selected_items()
	$'VBox/body_texture/ItemList'.clear()
	FillItemList()
	if len(index_pos)>0:
		$'VBox/body_texture/ItemList'.select(index_pos[0])


func _on_ItemList_item_selected(index):
	if not $"/root".get_node_or_null('space'):
		return
	var objekto
	for child in Global.fenestro_kosmo.get_children():
		if child.is_in_group('create'):
			if child.uuid == Global.objektoj[index]['uuid']:
				objekto = child
				break
	if objekto:
		Global.fenestro_kosmo.find_node('objekto').set_objekto(objekto)


