extends "res://kerno/fenestroj/tipo_a1.gd"


var konservejo # склад

# serĉo - поиск
func sercxo_konservejo(sxipo):
	# нужно работать с direktebla_objekto
	for ligilo in sxipo['ligilo']['edges']:
		if ligilo['node']['ligilo']['resurso']['objId'] == 5 or \
			ligilo['node']['ligilo']['resurso']['objId'] == 19:
#			if Global.logs:
#				print('===склад ==',ligilo['node']['ligilo'])
			return ligilo['node']['ligilo']
	return null


func load_konservejo():
	if !Global.fenestro_kosmo:
		print('konservejo - нет космоса')
		return
	var sxipo = Global.fenestro_kosmo.get_node('ship')
	if not sxipo:
		return
	# находим склад
#	konservejo = sercxo_konservejo(sxipo)
	konservejo = sercxo_konservejo(Global.direktebla_objekto[Global.realeco-2])
	if !konservejo:
		print('упс, склад не нашли :-(')
		return
	FillItemList()


# выводим, что на складе 
func FillItemList():
	if !konservejo:
		return
	var _items = get_node("VBox/body_texture/ItemList")
	_items.clear()
	var i = 1
	var volumeno = 0
	# print('===konservejo=',konservejo.uuid)
	# enteno - содержа́ние (одной субстанции в другой)
	for enteno in konservejo['ligilo']['edges']:
		# print('===enteno=',enteno['node']['ligilo']['nomo']['enhavo'])
		if enteno['node']['tipo']['objId']==3: # Находится внутри
			_items.add_item(String(i) + ') ' + String(enteno['node']['ligilo']['nomo']['enhavo']) + 
				', внутренний объём:' + String(enteno['node']['ligilo']['volumenoInterna']) + 
				', внешний объём:' + String(enteno['node']['ligilo']['volumenoEkstera']) + 
				', объём хранения:' + String(enteno['node']['ligilo']['volumenoStokado'])
			)
			volumeno += enteno['node']['ligilo']['volumenoStokado']
			i += 1
	if konservejo['volumenoInterna']:
		_items.add_item('Всего места: ' + String(konservejo['volumenoInterna']))
		_items.add_item('Свободно: ' + String(konservejo['volumenoInterna'] - volumeno))
