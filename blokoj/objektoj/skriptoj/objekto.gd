extends "res://kerno/fenestroj/tipo_a1.gd"


var objekto # описываемый объект
var eye = false # камера наведена на данный объект

# перезагружаем список описаний объектоа
func set_objekto(objekt):
	objekto = objekt
	if Global.fenestro_kosmo.get_node("camera").point_of_interest == objekto:
		eye = true
	else:
		eye = false
	FillItemList()
	$VBox.set_visible(true)


# выводим описание объекта
func FillItemList():
	$"VBox/body_texture/ItemList".clear()
	var _items = get_node("VBox/body_texture/ItemList")
	_items.add_item('имя объекта ' + objekto.objekto['nomo']['enhavo'])
	_items.add_item('uuid ' + objekto.objekto['uuid'])
	if objekto.objekto['integreco']:
		_items.add_item('целостность объекта ' + String(objekto.objekto['integreco']))
	if objekto.objekto['posedantoId']:
		_items.add_item('владелец объекта ID = ' + String(objekto.objekto['posedanto']['edges'].front()['node']['posedantoUzanto']['siriusoUzanto']['objId']))
		_items.add_item('ник владельца = ' + objekto.objekto['posedanto']['edges'].front()['node']['posedantoUzanto']['retnomo'])


func _on_eye_gui_input(event):
	if not objekto:
		return
	if event is InputEventMouseButton and event.pressed and event.button_index==1:
		Global.fenestro_kosmo.get_node("camera").choose = true
		if not eye:
			Global.fenestro_kosmo.get_node("camera").set_point_of_interest(objekto, true)
		else:
			Global.fenestro_kosmo.get_node("camera").set_point_of_interest(
				Global.fenestro_kosmo.get_node("ship"), true)
		eye = !eye
		get_tree().set_input_as_handled() 




