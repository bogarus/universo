extends Node
# Здесь будем хранить глобальные (используемые в нескольких блоках) запросы к бэкэнду


# Изменение статуса задачи, по умолчанию - завершение задачи (или другой статус)
func finado_tasko(tasko_uuid, statusoId = Net.statuso_fermo, id=0):
	if !id:
		id = Net.get_current_query_id()
		Net.net_id_clear.push_back(id)
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation ($uuid:UUID,  '+
		' $statusoId:Int, )'+
		'{ redaktuUniversoTaskoj (uuid: $uuid,  '+
		' statusoId:$statusoId) { status '+
		' message universoTaskoj { uuid } } }',
		'variables': {"uuid":tasko_uuid, "statusoId": statusoId } }})
#	if Global.logs:
#		print('===finado_tasko==',query)
	return query


# завершение проекта
func finado_projeko(projekto_uuid, id=0):
	var statusoId = 4 # Закрыт 
	if !id:
		id = Net.get_current_query_id()
		Net.net_id_clear.push_back(id)
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation ($projekto_uuid:UUID, '+
		' $statusoId:Int, )'+
		'{ redaktuUniversoProjekto (uuid: $projekto_uuid,  '+
		' statusoId:$statusoId) { status '+
		' message universoProjekto { uuid } } '+
		'}',
		'variables': {"statusoId": statusoId, "projekto_uuid":projekto_uuid } }})
	return query


# завершение задачи и проекта
func finado_projekto_tasko(projekto_uuid, tasko_uuid, id=0):
	var statusoId = 4
	if !id:
		id = Net.get_current_query_id()
		Net.net_id_clear.push_back(id)
	return JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation ($tasko_uuid:UUID, $projekto_uuid:UUID, '+
		' $statusoId:Int, ) {'+
		'redaktuUniversoProjekto (uuid: $projekto_uuid,  '+
		' statusoId:$statusoId) { status '+
		' message universoProjekto { uuid } } '+
		'redaktuUniversoTaskoj (uuid: $tasko_uuid,  '+
		' statusoId:$statusoId) { status '+
		' message universoTaskoj { uuid } } '+
		'}',
		'variables': {"tasko_uuid":tasko_uuid, "statusoId": statusoId, "projekto_uuid":projekto_uuid } } })


