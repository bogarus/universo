extends "res://kerno/fenestroj/tipo_a1.gd"


onready var _button = $VBox/kosmo
onready var _label = $VBox/body_texture/Label


const QueryObject = preload("res://kerno/menuo/skriptoj/queries.gd")


# ищем в списке объектов объект с конкретным uuid
func search_objekt_uuid(uuid):
	for objekt in Global.objektoj:
		if objekt['uuid']==uuid:
			return objekt['nomo']['enhavo']
	return 'не найден'


func print_button():
	_label.text = ''
	if Global.realeco==1:
		_button.disabled=true
		_button.set_visible(false)
		return
	if Global.direktebla_objekto[Global.realeco-2]['kosmo']:
		_button.text='Войти в станцию'
		_button.disabled=true
		_button.set_visible(false)
	else:
		if Global.direktebla_objekto[Global.realeco-2].get('ligiloLigilo'):
			if len(Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'])>0:
				var uuid = Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['uuid']
				if uuid:
					_label.text = Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['nomo']['enhavo']
#					search_objekt_uuid(uuid)# Global.direktebla_objekto[Global.realeco-2]
					_button.text='Выйти в космос'
					if Title.id_ligilo:
						_button.disabled=true
					else:
						_button.disabled=false
					_button.set_visible(true)


func _on_kosmo_pressed():
	if Global.direktebla_objekto[Global.realeco-2]['kosmo']:
		go_kosmostacioj()
	else:
		go_kosmo()
	$VBox.set_visible(false)

# вход в ближайшую станцию (задача - вычислить её в списке объектов)
func go_kosmostacioj():
	pass


func go_kosmo():
	if not Global.direktebla_objekto[Global.realeco-2].has('uuid'):
		print('Нет корабля для этого мира')
		return
	if len(Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'])>0:
		# отправляем задачу на выход из станции
		var celo = Vector3(Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'].front()['node']['posedanto']['koordinatoX'],
			Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'].front()['node']['posedanto']['koordinatoY'],
			Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'].front()['node']['posedanto']['koordinatoZ'])
		var dist = 0
		Global.fenestro_itinero.okazigi_itinero(
			Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['uuid'],
			'Выход из станции', #'nomo'
			'Выход из станции ' + Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['nomo']['enhavo'],
			celo.x,
			celo.y,
			celo.z,
			Transform(Basis.IDENTITY, celo),
			dist,
			Net.kategorio_eliro_kosmostacio,
			false
		)


