extends Node


const QueryObject = preload("res://kerno/menuo/skriptoj/queries.gd")

# сигнал загрузки объектов
signal load_objekto()


var load_scene : String
var id_direktebla_query # под каким номером запроса отправили запрос на управляемый список
var id_direktebla_kosmo_query # под каким номером запроса отправили запрос на управляемый список в космосе
var id_objekto# id запроса на объекты
var id_ligilo = 0 # id запроса на   выход из станции/заход в станцию
var ligilo_subscription_id = 0 # подписка на станцию
var id_statio = 0 # запрос на данные по станции


func _ready():
	var config = ConfigFile.new()
	var err = config.load("res://settings.cfg")
	if err == OK:
		Global.server = config.get_value("global", "server", false)
		Global.autoload = config.get_value("global", "autoload", false)
		Global.logs = config.get_value("global", "logs", false)
	if Global.logs:
		print('запуск res://kerno/menuo/menuo.tscn')
# warning-ignore:return_value_discarded
	Net.connect("connection_failed", self, "_on_connection_failed")
# warning-ignore:return_value_discarded
	Net.connect("connection_succeeded", self, "_on_connection_success")
# warning-ignore:return_value_discarded
	Net.connect("server_disconnected", self, "_on_server_disconnect")
# warning-ignore:return_value_discarded
	Net.connect("input_data", self, "_on_data_start")
	# выводим версию программы
	$CanvasLayer/Label.text = 'Universo version '+String(Global.numero_versio)+'.'+\
		String(Global.numero_subversio)+'.'+String(Global.numero_korektado)+'-alfo'
	get_tree().set_auto_accept_quit(false) # не даст закрыться программе


func _notification(what):
	if what == NOTIFICATION_WM_QUIT_REQUEST:
		_on_eliro_button_up()

func go_realeco():
	if Global.realeco == 2:
		on_com()
	elif Global.realeco == 1:
		on_real()
	elif Global.realeco == 3:
		on_cap()


# Обработчик сигнала "connection_succeeded"
func _on_connection_success():
	var q = QueryObject.new()
	id_direktebla_query = Net.get_current_query_id()
	Net.send_json(q.get_direktebla_json(Net.statuso_laboranta, Net.tasko_tipo_objekto, id_direktebla_query))


# закрываем признаки загрузки данных
func fermo_signo(): # закрыть признак
	# если ещё загрузки не было, то и отключать нечего
	if not Global.loading:
		return
	Global.loading = false
	#отключаем перехват сигнала
	Net.disconnect("input_data", self, "_on_data")
	Net.data_server.clear()
# warning-ignore:return_value_discarded
	Net.connect("input_data", self, "_on_data_start")
	# снимаем признаки загрузки параллельных миров
	for obj in Global.direktebla_objekto:
		obj['kosmo'] = Global.server # server всегда в космосе или нет изначально


# Обработчик сигнала "connection_failed"
func _on_connection_failed():
	fermo_signo()
	if !Global.server:
		_on_real_pressed()
	pass


# Обработчик сигнала "server_disconnected"
func _on_server_disconnect():
	fermo_signo()
	if !Global.server:
		_on_real_pressed()
	pass

# обработчик сигнала прихода данных при старте программы
func _on_data_start():
	var i = 0
	for on_data in Net.data_server:
		# ответ на запрос списка управляемый объектов
		if on_data['id'] == String(id_direktebla_query):
			for objekt in on_data['payload']['data']['universoObjekto']['edges']:
				Global.direktebla_objekto[objekt['node']['realeco']['objId']-2]=objekt['node'].duplicate(true)
				Global.direktebla_objekto[objekt['node']['realeco']['objId']-2]['kosmo'] = Global.server
			# теперь загружаем те объекты, которые из представленных находятся в космосе
			var q = QueryObject.new()
			id_direktebla_kosmo_query = Net.current_query_id
			Net.current_query_id += 1
			Net.send_json(q.get_direktebla_kosmo_json(id_direktebla_kosmo_query))
			#обработали запрос и удалили обработанную запись
			Net.data_server.remove(i)
		# список управляемый объектов в космосе
		elif on_data['id'] == String(id_direktebla_kosmo_query):
			if on_data['payload'].get('data'):
				var uuid = []
				for pars in on_data['payload']['data']['filteredUniversoObjekto']['edges']:
					uuid.append(pars['node']['uuid'])
				if !Global.server:
					for objekt in Global.direktebla_objekto:
						if objekt.has('uuid'):
							if objekt['uuid'] in uuid:
								objekt['kosmo']=true
				Global.loading = true
			#обработали запрос и удалили обработанную запись
			Net.data_server.remove(i)
			#отключаем перехват сигнала
			Net.disconnect("input_data", self, "_on_data_start")
# warning-ignore:return_value_discarded
			Net.connect("input_data", self, "_on_data")
			Global.objektoj.clear()
			load_objektoj_kosmo()
			#отображение и включение аудиоплеера
			if Global.status and Global.nickname:
				var gramofono = load("res://blokoj/gramofono/scenoj/gramofono.tscn")
				var gramofono_child = gramofono.instance()
				add_child(gramofono_child)
			print('загрузили данные')
			$CanvasLayer/UI/Taskoj/Window.mendo_informoj_projekto()
			if (Global.server or Global.autoload) and Global.realeco!=1:
				go_realeco()
		i += 1


# обработчик прихода постоянных данных
func _on_data():
	var i_data_server = 0
	for on_data in Net.data_server:
		if !on_data['payload']['data']:
			print('=== error ===',on_data)
		elif on_data['id'] == String(id_ligilo):
			# вошли в станцию
			Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']=\
				on_data['payload']['data']['universoObjekto']['edges'].front()['node']['ligiloLigilo'].duplicate(true)
			id_ligilo = 0
			Net.data_server.remove(i_data_server)
			# включаем подписку на связь со станцией
			subscribtion_ligilo(on_data['payload']['data']['universoObjekto']['edges'].front()['node']['ligiloLigilo']['edges'].front()['node']['uuid'])
		elif on_data['id'] == String(ligilo_subscription_id):
			# выходим из станции
			if on_data['payload']['data']['universoLigiloEventoj']['ligilo']['forigo']:
				# отписываемся
				var q = QueryObject.new()
				var id = Net.get_current_query_id()
				Net.net_id_clear.append(id)
				Net.send_json(q.nuligo_subscribtion(id))
				ligilo_subscription_id = 0
				# выходим из станции
				Global.direktebla_objekto[Global.realeco-2]['koordinatoX'] = \
					Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['koordinatoX'] + 120
				Global.direktebla_objekto[Global.realeco-2]['koordinatoY'] = \
					Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['koordinatoY'] + 120
				Global.direktebla_objekto[Global.realeco-2]['koordinatoZ'] = \
					Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['koordinatoZ'] + 200
				Global.kubo = Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['kubo']['objId']
				Global.direktebla_objekto[Global.realeco-2]['rotaciaX'] = 0
				Global.direktebla_objekto[Global.realeco-2]['rotaciaY'] = 0
				Global.direktebla_objekto[Global.realeco-2]['rotaciaZ'] = 0
				#удаляем в массиве объектов пользователя указатель на станцию
				Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'].clear()
				Global.direktebla_objekto[Global.realeco-2]['kosmo'] = true
				Title.CloseWindow()
				var err = get_tree().change_scene('res://blokoj/kosmo/scenoj/space.tscn')
				Title.reloadWindow()
				if err:
					print('Ошибка загрузки сцены: ', err)
			Net.data_server.remove(i_data_server)
		elif on_data['id'] == String(id_statio):
			id_statio = 0
			# пришли данные по станции
			if Global.fenestro_stacio:
				if on_data['payload']['data'].get('universoObjekto'):
					Global.fenestro_stacio.ligilo = []
					Global.fenestro_stacio.objekto = on_data['payload']['data']['universoObjekto']['edges'].front()['node'].duplicate(true)
				# добавляем связи по объектам
				for ligilo in on_data['payload']['data']['universoObjektoLigilo']['edges']:
					Global.fenestro_stacio.ligilo.append(ligilo['node'].duplicate(true))
				if on_data['payload']['data']['universoObjektoLigilo']['pageInfo']['hasNextPage']:
					load_stacio(Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['uuid'],
						on_data['payload']['data']['universoObjektoLigilo']['pageInfo']['endCursor'])
#				else:
#
#					print('=== больше нет связей ===')
			else:
				print('Станция не загрузилась!!!')
			Net.data_server.remove(i_data_server)
		elif on_data['payload']['data'].get('filteredUniversoObjekto'):
			# загрузка объектов
			var i = 0
			for item in on_data['payload']['data']['filteredUniversoObjekto']['edges']:
				if item['node']['uuid'] == Global.direktebla_objekto[Global.realeco-2]['uuid']:
					# обновляем данные directebla по своему кораблю
					var kosmo = Global.direktebla_objekto[Global.realeco-2]['kosmo']
					Global.direktebla_objekto[Global.realeco-2]=item['node'].duplicate(true)
					if Global.server:
						Global.direktebla_objekto[Global.realeco-2]['kosmo'] = true
					else:
						Global.direktebla_objekto[Global.realeco-2]['kosmo'] = kosmo
				else:# свой корабль не добавляем в список
					Global.objektoj.append(item['node'].duplicate(true))
					Global.objektoj[i]['distance'] = 0
					i += 1
			Net.data_server.remove(i_data_server)
			if on_data['payload']['data']['filteredUniversoObjekto']['pageInfo']['hasNextPage']:
				load_objektoj_kosmo(on_data['payload']['data']['filteredUniversoObjekto']['pageInfo']['endCursor'])
			else:
				if $"/root".get_node_or_null('space'):# если загружен космос
					$"/root".get_node('space').emit_signal("load_objektoj")# загружаем объекты космоса
				emit_signal("load_objekto")
		i_data_server += 1


func _on_Profilo_pressed():
# warning-ignore:return_value_discarded
	$CanvasLayer/UI/Popup_profilo.popup()
	$CanvasLayer/UI/Popup_profilo/profilo1/VBox.set_visible(true)

func _on_Objektoj_pressed():
	$CanvasLayer/UI/Objektoj/Window/VBox.set_visible(true)


func set_visible(visible: bool):
	$CanvasLayer/UI.visible = visible
	

func _on_Taskoj_pressed():
	$CanvasLayer/UI/Taskoj/Window/VBox.set_visible(true)

func CloseWindow():
	$CanvasLayer/UI/Taskoj/Window/VBox.set_visible(false)
	$CanvasLayer/UI/Objektoj/Window/VBox.set_visible(false)
	$CanvasLayer/UI/b_itinero/itinero/canvas/MarginContainer.set_visible(false)
	$CanvasLayer/UI/interago/interago/VBox.set_visible(false)


func reloadWindow():
	$CanvasLayer/UI/Taskoj/Window.mendo_informoj_projekto()
	Global.objektoj.clear()
	if Global.realeco>1:
		# если в космосе
		if Global.direktebla_objekto[Global.realeco-2]['kosmo'] or Global.server:
			load_objektoj_kosmo()
		else:
			load_stacio(Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'][0]['node']['posedanto']['uuid'],"")


# запрашиваем данные по связи с объектом
func objekto_link(uuid_objekto):
	# нужно получить в ответ uuid связи
	id_ligilo = Net.get_current_query_id()
	# Делаем запрос к бэкэнду
	var q = QueryObject.new()
	Net.send_json(q.get_objekto_uuid(
			uuid_objekto, 
			id_ligilo))


func on_cap():
	var mezo_cap = preload("res://kerno/menuo/resursoj/icons/tab4_3.png")
	$CanvasLayer/UI/mezo_regions.icon = mezo_cap
	$CanvasLayer/UI/real/real_cap_rect.set_visible(true)
	$CanvasLayer/UI/real/real_com_rect.set_visible(false)
	$CanvasLayer/UI/com/com_cap_rect.set_visible(true)
	$CanvasLayer/UI/com/com_real_rect.set_visible(false)
	$CanvasLayer/UI/cap/cap_com_rect.set_visible(false)
	$CanvasLayer/UI/cap/cap_real_rect.set_visible(false)
	$CanvasLayer/UI/Lbar.color = Color(0, 0.407843, 0.407843, 0.862745)
	$CanvasLayer/UI/Lbar3.self_modulate = Color(0, 0.407843, 0.407843, 0.862745)
	$CanvasLayer/UI/real/realLabel.set_visible(false)
	$CanvasLayer/UI/com/comLabel.set_visible(false)
	$CanvasLayer/UI/cap/capLabel.set_visible(true)
	$CanvasLayer/UI/romb.color = Color(0, 0.407843, 0.407843, 0.862745)
	# если объекта не будет в космосе, то загружать станцию
	if Global.direktebla_objekto[Global.realeco-2]['kosmo'] or Global.server:
# warning-ignore:return_value_discarded
		get_tree().change_scene('res://blokoj/kosmo/scenoj/space.tscn')
	else:
		if Global.direktebla_objekto[Global.realeco-2].get('ligiloLigilo'):
			subscribtion_ligilo(Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'].front()['node']['uuid'])
# warning-ignore:return_value_discarded
		get_tree().change_scene('res://blokoj/kosmostacioj/CapKosmostacio.tscn')


func _on_cap_pressed():
	if Global.loading: #здесь вызывать задержку надо
		var reload = false
		if Global.realeco!=3:
			Global.realeco = 3
			reload = true
			# при переключении миров закрываем окна ресурсов, объектов, т.к. они расчитаны на конкретный мир
			CloseWindow()
			on_cap()
		if reload:
			reloadWindow()
	else:
		print('Ещё не загружено')


func on_com():
	var mezo_com = preload("res://kerno/menuo/resursoj/icons/tab4_1.png")
	$CanvasLayer/UI/mezo_regions.icon = mezo_com
	$CanvasLayer/UI/real/real_cap_rect.set_visible(false)
	$CanvasLayer/UI/real/real_com_rect.set_visible(true)
	$CanvasLayer/UI/com/com_cap_rect.set_visible(false)
	$CanvasLayer/UI/com/com_real_rect.set_visible(false)
	$CanvasLayer/UI/cap/cap_com_rect.set_visible(true)
	$CanvasLayer/UI/cap/cap_real_rect.set_visible(false)
	$CanvasLayer/UI/Lbar.color = Color(0.333333, 0, 0.066667, 0.862745)
	$CanvasLayer/UI/Lbar3.self_modulate = Color(0.333333, 0, 0.066667, 0.862745)
	$CanvasLayer/UI/real/realLabel.set_visible(false)
	$CanvasLayer/UI/com/comLabel.set_visible(true)
	$CanvasLayer/UI/cap/capLabel.set_visible(false)
	$CanvasLayer/UI/romb.color = Color(0.333333, 0, 0.066667, 0.862745)
	# если объекта не будет в космосе, то загружать станцию
	if Global.direktebla_objekto[Global.realeco-2]['kosmo'] or Global.server:
# warning-ignore:return_value_discarded
		get_tree().change_scene('res://blokoj/kosmo/scenoj/space.tscn')
	else:
		if Global.direktebla_objekto[Global.realeco-2].get('ligiloLigilo'):
			subscribtion_ligilo(Global.direktebla_objekto[Global.realeco-2]['ligiloLigilo']['edges'].front()['node']['uuid'])
# warning-ignore:return_value_discarded
		get_tree().change_scene('res://blokoj/kosmostacioj/CapKosmostacio.tscn')
#		get_tree().change_scene("res://blokoj/kosmostacioj/ComKosmostacio.tscn")


func _on_com_pressed():
	if Global.loading: #здесь вызывать задержку надо
		var reload = false
		if Global.realeco!=2:
			Global.realeco = 2
			reload = true
			# при переключении миров закрываем окна ресурсов, объектов, т.к. они расчитаны на конкретный мир
			CloseWindow()
			on_com()
		if reload:
			reloadWindow()
	else:
		print('Ещё не загружено')


func on_real():
	var mezo_real = preload("res://kerno/menuo/resursoj/icons/tab4_2.png")
	$CanvasLayer/UI/mezo_regions.icon = mezo_real
	$CanvasLayer/UI/real/real_cap_rect.set_visible(false)
	$CanvasLayer/UI/real/real_com_rect.set_visible(false)
	$CanvasLayer/UI/com/com_cap_rect.set_visible(false)
	$CanvasLayer/UI/com/com_real_rect.set_visible(true)
	$CanvasLayer/UI/cap/cap_com_rect.set_visible(false)
	$CanvasLayer/UI/cap/cap_real_rect.set_visible(true)
	$CanvasLayer/UI/Lbar.color = Color(0.4, 0.6, 1, 0.862745)
	$CanvasLayer/UI/Lbar3.self_modulate = Color(0.4, 0.6, 1, 0.862745)
	$CanvasLayer/UI/real/realLabel.set_visible(true)
	$CanvasLayer/UI/com/comLabel.set_visible(false)
	$CanvasLayer/UI/cap/capLabel.set_visible(false)
	$CanvasLayer/UI/romb.color = Color(0.4, 0.6, 1, 0.862745)
	var err = get_tree().change_scene("res://blokoj/kosmostacioj/Kosmostacio.tscn")
	if err:
		print('ошибка смены сцены = ',err)


func _on_real_pressed():
	var reload = false
	if Global.realeco!=1:
		Global.realeco = 1
		reload = true
		# при переключении миров закрываем окна ресурсов, объектов, т.к. они расчитаны на конкретный мир
		CloseWindow()
		on_real()
# warning-ignore:return_value_discarded
	if reload:
		reloadWindow()


func _on_b_itinero_pressed():
	$CanvasLayer/UI/b_itinero/itinero/canvas/MarginContainer.set_visible(true)


func _on_ad_pressed():
	$CanvasLayer/UI/ad/ad_1/CanvasLayer/Margin.set_visible(true)


func _on_interago_pressed():
	if $CanvasLayer/UI/interago/interago/VBox.visible:
		$CanvasLayer/UI/interago/interago/VBox.set_visible(false)
	else:
		$CanvasLayer/UI/interago/interago.print_button()
		$CanvasLayer/UI/interago/interago/VBox.set_visible(true)


func _on_eliro_button_up():
	$CanvasLayer/UI/eliro/eliro.popup_centered()


func _input(event):
	if event is InputEvent and Input.is_key_pressed(KEY_ESCAPE):
		$CanvasLayer/UI/eliro/eliro.popup_centered()


# запрашиваем объекты, которые в космосе
func load_objektoj_kosmo(after=""):
	var q = QueryObject.new()
	id_objekto = Net.get_current_query_id()
#	зугрузка объектов партиями, количество в партии указано в константе запроса
	Net.send_json(q.get_objekto_kosmo(Net.statuso_laboranta, Net.tasko_tipo_objekto, Global.kubo, id_objekto, after))


# запрашиваем данные по станции
func load_stacio(uuid_statio, after):
	var q = QueryObject.new()
	id_statio = Net.get_current_query_id()
	Net.send_json(q.get_objekto(uuid_statio,id_statio,after))


func _on_komerco_button_up():
	$CanvasLayer/UI/komerco/komerco/VBox.set_visible(true)


func _on_konservejo_pressed():
	if $CanvasLayer/UI/konservejo/konservejo/VBox.visible:
		$CanvasLayer/UI/konservejo/konservejo/VBox.set_visible(false)
	else:
		$CanvasLayer/UI/konservejo/konservejo/VBox.set_visible(true)




func _on_indicoj_pressed():
	if $CanvasLayer/UI/indicoj/indicoj/VBox.visible:
		$CanvasLayer/UI/indicoj/indicoj/VBox.set_visible(false)
	else:
		$CanvasLayer/UI/indicoj/indicoj/VBox.set_visible(true)


func _on_items_pressed():
	if $CanvasLayer/UI/items/items_menuo.visible:
		$CanvasLayer/UI/items/items_menuo.set_visible(false)
	else:
		$CanvasLayer/UI/items/items_menuo.set_visible(true)
		$CanvasLayer/UI/items/items_menuo.plenigi_formularon()


# подписка на связь со станцией
func subscribtion_ligilo(uuid_ligilo):
	var q = QueryObject.new()
	ligilo_subscription_id = Net.get_current_query_id()
	if Global.logs:
		print('==установлена подписка для выхода из станции = ',ligilo_subscription_id)
	Net.send_json(q.subscription_ligilo(uuid_ligilo, ligilo_subscription_id))

