extends Object
# Здесь будем хранить всё для запросов к бэкэнду по блоку "rajtigo"


# количество объектов для загрузки
const count_objekto = "5"


# запрос на список управляемых объектов для вебсокета
func get_direktebla_json(statusoId, tipoId, id=0):
	if !id:
		id = Net.get_current_query_id()
	var statusoIdIn = String(Net.statuso_nova) + ', '+String(Net.statuso_laboranta) + ', '+String(Net.status_pauzo)
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload': {"query": "query ($UzantoId:Int, $statusoId:Float, "+
		" $tipoId:Float, $statusoIdIn:String)"+
		"{ universoObjekto (" +
		" universoobjektouzanto_Isnull:false,"+
		" universoobjektouzanto_Autoro_SiriusoUzanto_Id:$UzantoId," +
		") { edges { node { uuid integreco volumenoInterna volumenoEkstera volumenoStokado" +
		" projekto (statuso_Id: $statusoId, tipo_Id: $tipoId){ "+
		"  edges { node { uuid "+
		"  kategorio {edges {node {objId nomo{enhavo}}}}" +
		"  tasko (statuso_Id_In:$statusoIdIn){ edges {node { "+
		"   uuid finKoordinatoX finKoordinatoY finKoordinatoZ "+
		"   objekto{uuid} " +
		"   nomo{enhavo} priskribo{enhavo} " +
		"   kategorio {edges {node {objId nomo{enhavo}}}}" +
		"   pozicio statuso {objId } } } } } } } "+
		" nomo { enhavo } priskribo { enhavo } "+
		" resurso { objId nomo { enhavo } priskribo { enhavo } "+
		"  tipo { objId nomo { enhavo } } "+
		" } "+
		" koordinatoX koordinatoY koordinatoZ "+
		' posedantoObjekto '+
		'  { uuid } '+
		" stato{objId potenco statoAntaua {integreco} "+
		"    statoSekva{integreco}} " +
		" ligiloLigilo{edges{node{ uuid "+
		"  posedanto{ kubo {objId} koordinatoX koordinatoY " +
		" koordinatoZ uuid nomo {enhavo} }}}}" +
		" ligilo{edges{node{ uuid"+
		"  posedantoStokejo {uuid}" +
		"  konektiloPosedanto konektiloLigilo tipo{objId}"+
		"  ligilo{ uuid integreco volumenoInterna volumenoEkstera volumenoStokado nomo{enhavo} resurso{objId} "+
		"   stato{objId potenco statoAntaua {integreco} "+
		"    statoSekva{integreco}}" +
		"   ligilo{edges{node{ uuid" +
		"    posedantoStokejo {uuid}" +
		"    konektiloPosedanto konektiloLigilo tipo{objId} " +
		"    ligilo{ uuid integreco volumenoInterna volumenoEkstera volumenoStokado "+
		"     nomo{enhavo} resurso{objId} }}}}}" +
		"    tipo{objId}}}}" +
		" realeco{objId}" +
		" posedanto{edges{node{" +
		"  posedantoUzanto{ retnomo siriusoUzanto{ objId}}}}}" +
		" rotaciaX rotaciaY rotaciaZ } } } }",
		'variables': {"statusoId":statusoId, 
		"tipoId":tipoId, "statusoIdIn":statusoIdIn,
		"UzantoId":Global.id} } })
	# if Global.logs:
	# 	print("=== get_direktebla_query = ",query)
	return query


# запрос на восходящую связь по конкретному объекту (внутри какого объекта находится)
func get_objekto_uuid(uuid_objekto, id):
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload': {"query": "query ($uuid_objekto:UUID)"+
		"{ universoObjekto (" +
		" uuid:$uuid_objekto" +
		") { edges { node { " +
		" ligiloLigilo{edges{node{ uuid "+
		"  posedanto{ kubo {objId} koordinatoX koordinatoY " +
		"  koordinatoZ uuid nomo {enhavo} }}}}" +
		" } } } }",
		'variables': {"uuid_objekto":uuid_objekto, 
		} } })
	# if Global.logs:
	# 	print("=== get_objekto_uuid = ",query)
	return query


# запрос на список управляемых объектов в космосе
func get_direktebla_kosmo_json(id=0):
	if !id:
		id = Net.get_current_query_id()
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload': { "query": "query ($UzantoId:Int )"+
	"{ filteredUniversoObjekto (" +
	" universoobjektouzanto_Isnull:false,"+
	" universoobjektouzanto_Autoro_SiriusoUzanto_Id:$UzantoId," +
	" koordinatoX_Isnull:false, koordinatoY_Isnull:false, koordinatoZ_Isnull:false," +
	" kubo_Isnull:false," +
	") { edges { node { uuid " +
	"  realeco{objId}}}}}",
	'variables': {"UzantoId":Global.id} } })
	# print("=== get_direktebla_kosmo_query = ",query)
	return query


# Запрос к API, выбираем объекты, которые в космосе
# statusoId - статус проекта (2=в работе)
# tipoId - тип проекта Универсо (2 - Для объектов)
func get_objekto_kosmo(statusoId, tipoId, kuboId=Global.kubo, id=0, after=""):
	if !id:
		id = Net.get_current_query_id()
	var statusoIdIn = String(Net.statuso_nova) + ', '+String(Net.statuso_laboranta) + ', '+String(Net.status_pauzo)
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ "query": "query ($kuboId:Float, $statusoId:Float, "+
		" $realecoId:Float, $tipoId:Float, $statusoIdIn: String) " +
		"{ filteredUniversoObjekto (realeco_Id:$realecoId, kubo_Id: $kuboId, "+
		" koordinatoX_Isnull:false, koordinatoY_Isnull:false, koordinatoZ_Isnull:false," +
		" first:"+count_objekto+', after: "'+after+'" ' +
		" ) { pageInfo { hasNextPage endCursor } edges { node { uuid posedantoId "+
		" integreco volumenoInterna volumenoEkstera volumenoStokado" +
		# проект стрельбы  с задачами прицеливания и ведения огня

		# проект движени
		# " projekto (statuso_Id: $statusoId, tipo_Id: $tipoId, "+
		# "   kategorio_Id:$kategorioMovado){ "+
		" projekto (statuso_Id: $statusoId, tipo_Id: $tipoId){ "+
		"  edges { node { uuid statuso{objId}"+
		"  kategorio {edges {node {objId nomo{enhavo}}}}" +
		"  tasko (statuso_Id_In: $statusoIdIn){ edges {node { "+
		"   uuid finKoordinatoX finKoordinatoY finKoordinatoZ "+
		"   objekto{uuid} " +
		"   nomo{enhavo} priskribo{enhavo} " +
		"   kategorio {edges {node {objId nomo{enhavo}}}}" +
		"   pozicio statuso {objId} } } } } } } "+
		" nomo { enhavo } priskribo { enhavo } "+
		" resurso { objId nomo { enhavo } priskribo { enhavo } "+
		"  tipo { objId nomo { enhavo } } "+
		" } "+
		" koordinatoX koordinatoY koordinatoZ "+
		' posedantoObjekto '+
		'  { uuid } '+
		" stato{objId potenco statoAntaua {integreco} "+
		"    statoSekva{integreco}} " +
		" ligilo{edges{node{ uuid "+
		"  posedantoStokejo {uuid}" +
		"  konektiloPosedanto konektiloLigilo tipo{objId}"+
		"  ligilo{ uuid integreco volumenoInterna volumenoEkstera volumenoStokado nomo{enhavo} resurso{objId} "+
		"   stato{objId potenco statoAntaua {integreco} "+
		"    statoSekva{integreco}}" +
		"   ligilo{edges{node{ uuid " +
		"    posedantoStokejo {uuid}" +
		"    konektiloPosedanto konektiloLigilo tipo{objId} " +
		"    ligilo{ uuid integreco volumenoInterna volumenoEkstera volumenoStokado " +
		"     nomo{enhavo} resurso{objId} }}}}}" +
		"    tipo{objId}}}}" +
		" posedanto{edges{node{" +
		"  posedantoUzanto{ retnomo siriusoUzanto{ objId}}}}}" +
		" rotaciaX rotaciaY rotaciaZ } } } }",
		'variables': {"kuboId":kuboId, "statusoId":statusoId, 
		"tipoId":tipoId, "statusoIdIn":statusoIdIn,
		"realecoId":Global.realeco} } })
	# print('===get_objekto_kosmo=',query)
	return query


# Запрашиваем данные конкретного объекта и его связи по частям
func get_objekto(uuid, id=0, after=""):
	if !id:
		id = Net.get_current_query_id()
	var univesoObjekto = ""
	if !after:
		univesoObjekto = 	" universoObjekto ( uuid:$uuid ) "+\
		"  { edges { node { "+\
		"    uuid posedantoId  integreco volumenoInterna volumenoEkstera "+\
		"    volumenoStokado "+\
		"    nomo { enhavo } priskribo { enhavo } "+\
		"    resurso { objId nomo { enhavo } priskribo { enhavo } "+\
		"     tipo { objId nomo { enhavo } }  } "+\
		"    koordinatoX koordinatoY koordinatoZ  "+\
		"    posedantoObjekto   { uuid }  "+\
		"    stato{objId potenco statoAntaua {integreco} statoSekva{integreco}} "+\
		"    rotaciaX rotaciaY rotaciaZ } } } "
	
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ "query": "query ($uuid:UUID, $ligiloTipo:String) "+
	" { " + univesoObjekto +
	" universoObjektoLigilo( "+
	"  first:"+count_objekto+', after: "'+after+'" ' + 
	"  posedanto_Uuid:$uuid) "+
	"  {pageInfo { hasNextPage endCursor } "+
	"   edges{node{ uuid   posedantoStokejo {uuid} "+
	"    konektiloPosedanto konektiloLigilo tipo{objId} tipo{objId}"+
	"    ligilo{ uuid integreco volumenoInterna "+
	"      volumenoEkstera volumenoStokado nomo{enhavo} "+
	"      resurso{objId} "+
	"      stato{objId potenco statoAntaua {integreco} statoSekva{integreco}} "+
	"      posedanto{edges{node{ "+\
	"       posedantoUzanto{ retnomo siriusoUzanto{ objId}}}}} "+\
	"      ligilo(tipo_Id_In:$ligiloTipo){edges{node{ uuid "+
	"       posedantoStokejo {uuid} konektiloPosedanto konektiloLigilo tipo{objId} "+
	"       ligilo{ uuid integreco volumenoInterna volumenoEkstera "+
	"        volumenoStokado      nomo{enhavo} resurso{objId} }}}}} "+
	" }}} } ",
		'variables': {"uuid":uuid, "ligiloTipo":"1,2"} } }) # запрашиваем только связи для сборки объектов
	# if Global.logs:
	# 	print('===get_objekto=',query)
	return query


# отказ от подписки
func nuligo_subscribtion(id):
	var query = JSON.print({
		'type': 'stop',
		'id': '%s' % id})
	# if Global.logs:
	# 	print('=== nuligo_subscribtion =query= ',query)
	return query


# подписка на связь с объектом
func subscription_ligilo(ligilo, id):
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'subscription ($ligilo: String!)'+
		' { universoLigiloEventoj (ligilo: $ligilo) { '+
		' evento '+
		' ligilo { uuid forigo } '+
		' } } ',
		'variables': {"ligilo": ligilo } }})
	# if Global.logs:
	# 	print('=== subscription_ligilo =query= ',query)
	return query


